

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/ucontext.h>
#include "../include/support.h"
#include "../include/cthread.h"
#include "../include/cdata.h"
#define TAMANHO_PILHA 1000


//uma lista de s_TCB
/*typedef struct s_TCB {
	int		tid; 		// identificador da thread
	int		state;		// estado em que a thread se encontra
					// 0: Cria��o; 1: Apto; 2: Execu��o; 3: Bloqueado; 4: T�rmino; 5: Apto-Suspenso; 6: Bloqueado-Suspenso (n�o estamos usando)
	int 		prio;		// Prioridade associada a thread
	ucontext_t 	context;	// contexto de execu��o da thread (SP, PC, GPRs e recursos)
	void		*data;	// aponta estruturas de dados, associadas � thread, necess�rias � implementa��o

} TCB_t; */


FILA2 aptoAltaPrio, aptoBaixaPrio, aptoMediaPrio; //ponteiro para as listas de threads no estado APTO, cada uma representando uma prioridade
FILA2 bloq; //ponteiro para a lista de threads no estado BLOQUEADO
TCB_t* exec; //ponteiro para o TCB da thread atualmente sendo executada
ucontext_t* escalonador;//contexto do escalonador (para o qual todas as threads desviam quando terminam)
unsigned int id = 1;//vari�vel global para ser incrementada a cada vez que uma nova thread � criada e usada como seu identificador (come�a em 1 pq a thread 0 � a main)
int inicializado = 0;//flag que indica se j� foi criada a thread main
TCB_t* threadMain;//thread principal do processo
int stop = 0;

void cChaveamento(){
//analisa as listas de threads e faz um setcontext() para a de maior prioridade dispon�vel
//caso exista mais de uma com a mesma prioridade, escolhe a do in�cio da lista
printf("iniciando escalonamento...");
getchar();
if(stop == 1)exit(0);

TCB_t* nextThread = threadMain;
NODE2 N;
//se as tr�s listas estiverem vazias, faz setcontext(mainThread), caso contr�rio seleciona a primeira thread da lista n�o-vazia de maior prioridade
    if(FirstFila2(&aptoAltaPrio) == 0){
        nextThread = (TCB_t*)GetAtIteratorFila2(&aptoAltaPrio);
        DeleteAtIteratorFila2(&aptoAltaPrio);
    }
    else if(FirstFila2(&aptoMediaPrio) == 0){
        nextThread = (TCB_t*)GetAtIteratorFila2(&aptoMediaPrio);
        DeleteAtIteratorFila2(&aptoMediaPrio);

    } else if(FirstFila2(&aptoBaixaPrio) == 0){
        nextThread = (TCB_t*)GetAtIteratorFila2(&aptoBaixaPrio);
		//printf("\nvai comecar thread de id %d\n", nextThread->tid);
        DeleteAtIteratorFila2(&aptoBaixaPrio);

    }
    exec = nextThread;


setcontext(&(nextThread->context));
//nunca deve chegar aqui, se for o caso aconteceu algum erro
    printf("erro no chaveamento ");
}

int cthreadInit(){//cria a thread main (para que se possa retornar a ela quando necess�rio, etc) e o contexto do escalonador. Deve ser chamada apenas uma vez
    printf("\ninit\n");
    TCB_t* mainThread = malloc(sizeof(TCB_t));
    ucontext_t mainContexto;

    getcontext(&mainContexto);//copia o contexto atual para mainContexto
    mainContexto.uc_link=0; //nada deve acontecer ap�s a execu��o do contexto (j� que ele � a main)
    mainContexto.uc_stack.ss_sp=malloc(TAMANHO_PILHA);//aloca a pilha do contexto
    mainContexto.uc_stack.ss_size=TAMANHO_PILHA;//tamanho da pilha do contexto
    mainContexto.uc_stack.ss_flags=0;//flags da pilha do contexto (n�o � utilizado neste projeto, ent�o sempre � inicializado com 0)

    mainThread->tid = 0;
    mainThread->state = 0;
    mainThread->prio = 2;
    mainThread->context = mainContexto;
    mainThread->data = 0;

    escalonador = malloc(sizeof(ucontext_t));
    getcontext(escalonador);
    escalonador->uc_link = 0;
    escalonador->uc_stack.ss_sp=malloc(TAMANHO_PILHA);
    escalonador->uc_stack.ss_size=TAMANHO_PILHA;
    escalonador->uc_stack.ss_flags=0;
    makecontext(escalonador, cChaveamento, 0);

    CreateFila2(&aptoAltaPrio);
    CreateFila2(&aptoMediaPrio);
    CreateFila2(&aptoBaixaPrio);
    CreateFila2(&bloq);

    inicializado = 1;
    exec = mainThread;
    threadMain = mainThread;

    printf("\nend init\n");
    getcontext(&(mainThread->context));
    return 0;
}

int cInsereEmApto(TCB_t* thread){//insere thread na fila APTO correspondente � sua prioridade
	NODE2* n = malloc(sizeof(NODE2));
TCB_t* teste;
	n->node = thread;
	printf("\ntentando inserir em apto thread com prio %d\n",thread->prio);
    switch(thread->prio){
        case 0:
			printf("vai inserir na prirodade alta");
			AppendFila2(&aptoAltaPrio,n);
        break;
        case 1:
			printf("vai inserir na prirodade media");
			AppendFila2(&aptoMediaPrio,n);
        break;
        case 2:
			printf("vai inserir na prirodade baixa");
			AppendFila2(&aptoBaixaPrio,thread);
        break;
        default:
			return -1;
    }
printf("inseriu em apto com sucesso\n");

    return 0;
}

int ccreate (void* (*start)(void*), void *arg, int prio) {
    //cria (aloca) um novo TCB_t com as informa��es passadas, compara a prioridade da nova thread com a apontada por exec e age de acordo
    if(inicializado == 0) cthreadInit();

    ucontext_t novoContexto;
    TCB_t* novaThread = malloc(sizeof(TCB_t));
    if(novaThread == NULL || (prio != 0 && prio != 1 && prio != 2)){//erro de aloca��o de mem�ria ou prioridade inv�lida
        return -1;
    }


    getcontext(&novoContexto);//copia o contexto atual para novoContexto
    novoContexto.uc_link=escalonador; //sempre que uma thread termina ela deve desviar para o contexto do escalonador (� NESTE DESVIO QUE ACONTECE A FLOATING POINT EXCEPTION)
    novoContexto.uc_stack.ss_sp=malloc(TAMANHO_PILHA);//aloca a pilha do contexto
    novoContexto.uc_stack.ss_size=TAMANHO_PILHA;//tamanho da pilha do contexto
    novoContexto.uc_stack.ss_flags=0;//flags da pilha do contexto (n�o � utilizado neste projeto, ent�o sempre � inicializado com 0)

    if(novoContexto.uc_stack.ss_sp == NULL){//erro de aloca��o de mem�ria
        free(novaThread);//libera o espa�o alocado para a thread j� que deu erro
        return -1;
    }

    novaThread->tid = id;
    novaThread->state = 0;
    novaThread->prio = prio;
    makecontext(&novoContexto, *start, 0);
    novaThread->context = novoContexto;
    novaThread->data = 0;//a princ�pio n�o vamos usar este campo

    id++;
    //checar se � a main primeiro
	printf("\ncriou nova de id %d\n",novaThread->tid);
    if(exec->tid == 0){//se a thread executando for a main, necessariamente inicia a nova thread
        printf("\nesta executando main\n");
        exec = novaThread;
        swapcontext(&(threadMain->context), &(exec->context));
        //swapcontext(&(threadMain->context), &(novaThread->context));
    }
    else if(novaThread->prio < exec->prio){//se a thread executando n�o for a main e tiver prioridade menor que a nova, executa a nova e bota a atual em apto
        printf("\nnova tem prioridade maior\n");
		TCB_t* aux = exec;
        cInsereEmApto(exec);
        exec = novaThread;
        swapcontext(&(aux->context), &(exec->context));
    }
    else{//se n�o cair em nenhum dos casos, apenas coloca a nova thread em apto e retorna
		printf("\napenas insere em apto\n");        
		cInsereEmApto(novaThread);
    }
	printf("terminou de criar nova com id %d\n", novaThread->tid);
    return 0;
}

int csetprio(int tid, int prio) {
    if(!inicializado) cthreadInit();
	return -1;
}

int cyield(void) {
    if(!inicializado) cthreadInit();
	return -1;
}

int cjoin(int tid) {
    if(!inicializado) cthreadInit();
	return -1;
}

int csem_init(csem_t *sem, int count) {
    if(!inicializado) cthreadInit();
	return -1;
}

int cwait(csem_t *sem) {
    if(!inicializado) cthreadInit();
	return -1;
}

int csignal(csem_t *sem) {
    if(!inicializado) cthreadInit();
	return -1;
}

int cidentify (char *name, int size) {
    if(!inicializado) cthreadInit();
	strncpy (name, "Amaury Teixeira Cassola - 287704\n", size);//ADICIONAR OS NOMES E CART�ES DE TODOS OS AMIGUINHOS
	return 0;
}


//======================================================================================================================================
//PROGRAMA TESTE

void f2(int*  a){
    printf("\nesta eh a funcao de id %d\n",exec->tid);
    printf("\nfinalizando a de id %d\n", exec->tid);
}

void f1(int* a){
    printf("primeira que cria duas de prioridade 2\n");
    int b = 5;
    ccreate(f2, &b, 2);
	ccreate(f2, NULL, 2);
    printf("\nFINALIZANDO PRIMEIRA, AGORA VAI PRO ESCALONADOR\n");
}



int main(){
    int a = 3;
    printf("comecou main sei la\n");

    ccreate(f1,&a,1);
	
    printf("\nterminou programa sei la\n\n");
    return 0;

}
